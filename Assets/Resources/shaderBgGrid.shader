﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/shaderBgGrid"
{
    SubShader  
    {  
        // No culling or depth  
        Cull Off ZWrite Off ZTest Always  
  
        Pass  
    {  
        CGPROGRAM  
#pragma vertex vert  
#pragma fragment frag  
  
#include "UnityCG.cginc"  
  
        struct appdata  
    {  
        float4 vertex : POSITION;  
        float2 uv : TEXCOORD0;  
    };  
  
    struct v2f  
    {  
        float2 uv : TEXCOORD0;  
        float4 vertex : SV_POSITION;  
    };  
  
    v2f vert(appdata v)  
    {  
        v2f o;  
        o.vertex = UnityObjectToClipPos(v.vertex);  
        o.uv = v.uv;  
        return o;  
    }  
  
  
fixed4 frag(v2f i) : SV_Target  
    {  
        fixed3 backgroundColor = fixed3(1.0, 1.0, 1.0);  
    fixed3 axesColor = fixed3(0.0, 0.0, 1.0);  
    fixed3 gridColor = fixed3(0.5, 0.5, 0.5);  
  
    fixed3 pixel = backgroundColor;  
  
    const float tickWidth = 0.01;  
    for (float lc = 0.0; lc< 1.0; lc += tickWidth) {  
        if (abs(i.uv.x - lc) < 0.0005) pixel = gridColor;  
        if (abs(i.uv.y - lc) < 0.0005) pixel = gridColor;  
    }  
  
    // 画坐标轴  
    if (abs(i.uv.x)<0.005) pixel = axesColor;  
    if (abs(i.uv.y)<0.006) pixel = axesColor;  
  
    return fixed4(pixel, 0.5);  
    }   
        ENDCG  
    }  
    }  
}
