﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPlayerManager : MonoBehaviour {

    public static CPlayerManager s_Instance = null;

    Dictionary<int, Player> m_dicPlayers = new Dictionary<int, Player>();

    Dictionary<int, float> m_dicTotalEatArea = new Dictionary<int, float>();
    Dictionary<int, int> m_dicKillCount = new Dictionary<int, int>();
    Dictionary<int, int> m_dicbeKilledCount = new Dictionary<int, int>();

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddPlayer( Player player )
    {
        m_dicPlayers[player.GetOwnerId()] = player;
    }

    public Player GetPlayer( int nOwnerId )
    {
        Player player = null;
        m_dicPlayers.TryGetValue(nOwnerId, out player);
        return player;
    }

    public void RecordTotalEatArea(  int nPlayerId, float val )
    {
      
    }

    public void RecordWhoKillWhom( int nEaterId, int nDeadMeatId )
    {
        Player playerEater = GetPlayer(nEaterId);
        playerEater.IncKillCount();
        Player playerDeadMeat = GetPlayer(nDeadMeatId);
        playerDeadMeat.IncBeKilledCount();
    }

    public string GenerateOnePlayerInfo( Player player )
    {
        string szPlayerContent = "";
        szPlayerContent += "玩家名字      ：" + player.GetPlayerName() + "\n";
        szPlayerContent += "击杀次数      ：" + player.GetKillCount() + "\n";
        szPlayerContent += "被击杀次数   ：" + player.GetBeKilledCount() + "\n";
        szPlayerContent += "累计吃球体积：" + player.GetTotalEatArea() + "\n";
        szPlayerContent += "_____________________________________________________________________\n\n";

        return szPlayerContent;
    }

    string m_szShengFuPlayerName = "";
    public void SetShengFuPanDingPlayer(string szShengFuPlayerName)
    {
        m_szShengFuPlayerName = szShengFuPlayerName;
    }

    public string GetJieSuanInfo()
    {
        string szContent = "";

        szContent += "吃掉决胜球的玩家：" + m_szShengFuPlayerName + "\n\n";

        szContent += GenerateOnePlayerInfo(Main.s_Instance.m_MainPlayer);

        foreach( KeyValuePair<int, Player> pair in  m_dicPlayers )
        {
            if ( pair.Key == Main.s_Instance.m_MainPlayer.GetOwnerId())
            {
                continue;
            }
            szContent += GenerateOnePlayerInfo(pair.Value);
        }

        return szContent;
    }
}
