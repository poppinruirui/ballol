﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// dark_scale = circle_size * 0.6f

public class DarkForest : MonoBehaviour {

    public SpriteRenderer _NightRenderer;
    const int c_nMaskTotal = 1; // 最大支持15层混合，超过了shader就会报错。目前只采用1层。


    public Texture2D[] _TestMask = new Texture2D[c_nMaskTotal];

    List<Ball> m_lstBalls = new List<Ball>();

    static Color cTempColor = new Color();
    Vector3 vecTempPos = new Vector3();
    public Vector3 vecTempScale = new Vector3 ();

    public static DarkForest s_Instance = null;

    bool m_bIsOn = true;

    float m_BallSizeSortCount = 0.0f;

    void Awake()
    {
        s_Instance = this;

   

        ToggleDarkForestMode( false );
    }



    // Use this for initialization
    void Start () {
		
	}

    void GenerateBallList()
    {
        List<Ball> lstAllBalls = Main.s_Instance.m_MainPlayer.GetBallList();
        m_lstBalls.Clear();
        for ( int i = 0; i < lstAllBalls.Count; i++ ) 
        {
            Ball ball = lstAllBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            m_lstBalls.Add( ball );
        }
        SortByBallSize();

        int nBallIndex = 0;
        bool bDark = false;
        for ( int i = 1; i < 14; i++ )
        {
            Ball ball = null;
            if (nBallIndex < m_lstBalls.Count)
            {
                ball = m_lstBalls[nBallIndex++];
                if ( ball.GetClassId() == 1 )
                {
                    bDark = true;
                }
                vecTempPos.x = ball.transform.position.x / MapEditor.s_Instance.GetWorldSizeX() ;
                vecTempPos.y = ball.transform.position.y / MapEditor.s_Instance.GetWorldSizeY() ;
                float fScale = 5f / ball.GetSize();
                _NightRenderer.material.SetTextureScale("_Mask" + i, new Vector2(fScale, fScale));
                _NightRenderer.material.SetTextureOffset("_Mask" + i, new Vector2((1.0f - fScale) / 2f - vecTempPos.x * fScale, (1.0f - fScale) / 2.0f - vecTempPos.y * fScale));

            }
            else
            {
                vecTempScale.x = 10000f;
                vecTempScale.y = 10000f;
            }

          //_NightRenderer.material.SetTextureOffset("_Mask" + i, new Vector2(vecTempPos.x, vecTempPos.y));
            
        } // end for

        if (bDark)
        {
            ToggleDarkForestMode( true );
        }
        else
        {
           ToggleDarkForestMode( false );
        }

        g_fCircleScale = g_fShit * MapEditor.s_Instance.GetWorldSizeX() / CClassEditor.s_Instance.GetClassCircleRadius();
        _NightRenderer.material.SetTextureScale("_Mask" + 0, new Vector2(g_fCircleScale, g_fCircleScale));
       _NightRenderer.material.SetTextureOffset("_Mask" + 0, new Vector2((1.0f - g_fCircleScale) / 2f - vecTempPos.x * g_fCircleScale, (1.0f - g_fCircleScale) / 2.0f - vecTempPos.y * vecTempScale.y));
    }
    public float g_fShit = 1f;
    public float g_fCircleScale = 0.5f;
    // Update is called once per frame
    void Update () {
        /*
        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game || (!m_bIsOn))
        {
            return;
        }
        */

        if (Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null)
        {
            return;
        }

        GenerateBallList();

    }

    public void AddBall( Ball ball )
    {
        m_lstBalls.Add( ball );
        SortByBallSize();
    }


    public void ToggleDarkForestMode( bool isOn )
    {
        m_bIsOn = isOn;

        if (m_bIsOn)
        {
            vecTempPos.x = 0;
            vecTempPos.y = 0f;
            vecTempPos.z = -500f;
            this.transform.position = vecTempPos;
        }
        else
        {
            vecTempPos.x = -10000f;
            vecTempPos.y = -10000f;
            this.transform.position = vecTempPos;
        }
    }

    void SortByBallSize()
    {
        for ( int i = 0; i < m_lstBalls.Count - 1; i++ )
        {
            for ( int j = i + 1; j < m_lstBalls.Count; j++ )
            {
                Ball ball1 = m_lstBalls[i];
                Ball ball2 = m_lstBalls[j];
                
                if ( ball1.GetSize() < ball2.GetSize() )
                {
                    m_lstBalls[i] = ball2;
                    m_lstBalls[j] = ball1;
                }
            }
        }
    }


    public void SetDarkForestScale(float fDarkForstScaleX, float fDarkForstScaleY )
    {
        vecTempScale.x = fDarkForstScaleX;
        vecTempScale.y = fDarkForstScaleY;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;

    }
}
