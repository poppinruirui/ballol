﻿Shader "mask shader"
{
   Properties
   {
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Mask0 ("Culling Mask0", 2D) = "white" {}
	  _Mask1 ("Culling Mask1", 2D) = "white" {}
	  _Mask2 ("Culling Mask2", 2D) = "white" {}
	  	  _Mask3 ("Culling Mask3", 2D) = "white" {}
		  	  _Mask4 ("Culling Mask4", 2D) = "white" {}
			  	  _Mask5 ("Culling Mask5", 2D) = "white" {}
				  	  _Mask6 ("Culling Mask6", 2D) = "white" {}
					  	  _Mask7 ("Culling Mask7", 2D) = "white" {}
						  	  _Mask8 ("Culling Mask8", 2D) = "white" {}
							  	  _Mask9 ("Culling Mask9", 2D) = "white" {}
								  	  _Mask10 ("Culling Mask10", 2D) = "white" {}
									  	  _Mask11 ("Culling Mask11", 2D) = "white" {}
										  	  _Mask12 ("Culling Mask12", 2D) = "white" {}
											  	  _Mask13 ("Culling Mask13", 2D) = "white" {}
												  	  _Mask14("Culling Mask14", 2D) = "white" {}
													  	  _Mask15 ("Culling Mask15", 2D) = "white" {}
      _Cutoff ("Alpha cutoff", Range (0,1)) = 0.1

   } 

   SubShader
   {
      Tags {"Queue"="Transparent"}
      Lighting Off
      ZWrite Off
      Blend SrcAlpha OneMinusSrcAlpha
      AlphaTest GEqual [_Cutoff]

      Pass
      {
         SetTexture [_Mask0] {combine texture}
         SetTexture [_MainTex] {combine texture,texture-previous}
		 SetTexture [_Mask1]{ combine previous,previous-texture }
		 SetTexture [_Mask2]{ combine previous,previous-texture }
		 SetTexture [_Mask3]{ combine previous,previous-texture }
		 SetTexture [_Mask4]{ combine previous,previous-texture }
		 SetTexture [_Mask5]{ combine previous,previous-texture }
		 SetTexture [_Mask6]{ combine previous,previous-texture }
		 SetTexture [_Mask7]{ combine previous,previous-texture }
		 SetTexture [_Mask8]{ combine previous,previous-texture }
		 SetTexture [_Mask9]{ combine previous,previous-texture }
		 SetTexture [_Mask10]{ combine previous,previous-texture }
		 SetTexture [_Mask11]{ combine previous,previous-texture }
		 SetTexture [_Mask12]{ combine previous,previous-texture }
		 SetTexture [_Mask13]{ combine previous,previous-texture }
		 SetTexture [_Mask14]{ combine previous,previous-texture }
		

      }
   }
}