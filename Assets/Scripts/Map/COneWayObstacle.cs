﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COneWayObstacle : MonoBehaviour {

    float m_fLength = 0f;

    public GameObject _goArrow0;
    public GameObject _goArrow1;
    public LineRenderer _lrMain;

    static Vector3 vecTempPos = new Vector3();
    Vector3 m_vecPos = new Vector3();

    float m_fRotation;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLength( float val )
    {
        m_fLength = val;
    }

    public void SetRotation( float val )
    {
        m_fRotation = val;

        _goArrow0.transform.localRotation = Quaternion.identity;
        _goArrow0.transform.Rotate(0.0f, 0.0f, m_fRotation);

        _goArrow1.transform.localRotation = Quaternion.identity;
        _goArrow1.transform.Rotate(0.0f, 0.0f, m_fRotation);
    }

    public void SetPos( Vector3 pos )
    {
        m_vecPos = pos;

        _goArrow0.transform.position = m_vecPos;
        vecTempPos.x = m_vecPos.x + Mathf.Cos( m_fRotation * CyberTreeMath.c_fAngleToRadian) * m_fLength;
        vecTempPos.y = m_vecPos.y + Mathf.Sin (m_fRotation * CyberTreeMath.c_fAngleToRadian) * m_fLength;
        vecTempPos.z = 0f;
        _goArrow1.transform.position = vecTempPos;
    }

    public void SetBaseInfo( Vector3 pos, float rotation, float length )
    {
        SetLength(length);
        SetRotation(rotation);
        SetPos(pos);
    }
}
