﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMsgSystem : MonoBehaviour {

	public static CMsgSystem s_Instance;

	public Text[] _aryMsg;


    public enum eSysMsgType
    {
        mp_not_enough,             // 蓝不够
        cold_down_not_completed,   // ColdDown还没结束
        money_not_enough,          // 钱不够
    };

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowMsg( int nIdx, string szContent )
	{
		_aryMsg [nIdx].text = szContent;
	}

    public string GetMsgContentByType( eSysMsgType type )
    {
        string szContent = "";

        switch( type )
        {
            case eSysMsgType.mp_not_enough:
                {
                    szContent = "您没有足够的蓝释放该技能";
                }
                break;
            case eSysMsgType.cold_down_not_completed:
                {
                    szContent = "该技能还没有准备好";
                }
                break;
            case eSysMsgType.money_not_enough:
                {
                    szContent = "您没有足够的金币购买该技能";
                }
                break;
        }
        return szContent;
    }
}
