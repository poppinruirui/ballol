﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CJiShaInfo : MonoBehaviour {

    public static CJiShaInfo s_Instance;

    public float m_fShowTime = 5f;
    float m_fShowTimeCount = 0f;

    public GameObject _panelJiSha;
    public Text _txtKillerName;
    public Text _txtDeadMeatName;
    public Image _imgKillerAvatar;
    public Image _imgDeadMeatAvatar;

    bool m_bShow = false;

    // Use this for initialization
    void Start () {
		
	}

    private void Awake()
    {
        s_Instance = this;
    }

    // Update is called once per frame
    void Update () {
        Counting();

    }

    public void Show(string szKillerName, string szDeadMeatName, Sprite sprKillerAvatar, Sprite sprDeadMeatAvatar)
    {
        _panelJiSha.SetActive( true );
        _txtKillerName.text = szKillerName;
        _txtDeadMeatName.text = szDeadMeatName;
        _imgKillerAvatar.sprite = sprKillerAvatar;
        _imgDeadMeatAvatar.sprite = sprDeadMeatAvatar;
        m_bShow = true;
        m_fShowTimeCount = 0;
    }

    public void Hide()
    {
        m_bShow = false;
        _panelJiSha.gameObject.SetActive(false);
    }

    void Counting()
    {
        if ( !m_bShow)
        {
            return;
        }
        m_fShowTimeCount += Time.deltaTime;
        if (m_fShowTimeCount >= m_fShowTime)
        {
            Hide();
        }
    }
}
