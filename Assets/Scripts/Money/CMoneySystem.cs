﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMoneySystem : MonoBehaviour {

    public static CMoneySystem s_Instance;

    public Text _txtMoney;

    float m_fMoney = 0;
    float m_fCurShowValue = 0;
    float m_fDelta = 0;

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MoneyNumberChange();

    }

    public void SetMoney( int val )
    {
        m_fMoney = val;
        if (m_fMoney < 0)
        {
            m_fMoney = 0;
        }
        m_fCurShowValue = m_fMoney;
        _txtMoney.text = m_fCurShowValue.ToString("f0");
        // m_fDelta = (m_fMoney - m_fCurShowValue ) *Time.deltaTime;

        CItemSystem.s_Instance.RefreshAddPointButton();
    }

    public int GetMoney()
    {
        return (int)m_fMoney;
    }

    void MoneyNumberChange()
    {
        return;

        if ((int)m_fCurShowValue == (int)m_fMoney )
        {
            return;
        }

        if (  m_fCurShowValue <= 0f )
        {
            m_fCurShowValue = 0f;
            return;
        }

        m_fCurShowValue += m_fDelta;
        if ( Mathf.Abs(m_fMoney -m_fCurShowValue)  <= m_fDelta)
        {
            m_fCurShowValue = m_fMoney;
        }

        _txtMoney.text = m_fCurShowValue.ToString("f0");
    }
}
