using UnityEngine;

public class CtrlMode
{
    //public const int CTRL_MODE_NONE          = 0;        
	public const int CTRL_MODE_HAND          = 0;
    public const int CTRL_MODE_JOYSTICK      = 1;    
    public const int CTRL_MODE_SCREEN_CURSOR = 2;        
    public const int CTRL_MODE_WORLD_CURSOR  = 3;
    
    public const int DIR_INDICATOR_NONE  = 0;
    public const int DIR_INDICATOR_ARROW = 1;
    public const int DIR_INDICATOR_LINE  = 2;

	public static int _ctrl_mode = CtrlMode.CTRL_MODE_JOYSTICK;
    
    public static int _dir_indicator_type = CtrlMode.DIR_INDICATOR_LINE;

    const float _view_port_overflow = -0.05f;
    
    const float _joystick_enlarge_factor = 5.0f;
    
	public static float _ball_scale_zoom_factor = 1;//2.2f;

    public static float _world_port_h = 0.0f;
    public static float _world_port_v = 0.0f;
    
    public const float MIN_ORTHOGRAPHIC_SIZE = 20.0f;

    public static float _zoom_velocity = 0.0f;
    public static float _zoom_time = 0.5f;

    public static void SetCtrlMode(int mode)
    {
        if (mode == CtrlMode.CTRL_MODE_JOYSTICK ||
            mode == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
            mode == CtrlMode.CTRL_MODE_SCREEN_CURSOR) {
            ETCInput.SetControlActivated("Joystick", true);
        }
        else{
            ETCInput.SetControlActivated("Joystick", false);
        }
        CtrlMode._ctrl_mode = mode;
        CtrlMode.UpdateWorldPort();
    }

    public static void SetDirIndicatorType(int type)
    {
        CtrlMode._dir_indicator_type = type;
		if (Main.s_Instance && Main.s_Instance.m_MainPlayer) {
			Main.s_Instance.m_MainPlayer.UpdateIndicatorType (type);
		}
    }

    public static int GetDirIndicatorType()
    {
        return CtrlMode._dir_indicator_type;
    }
    
    public static void UpdateWorldPort()
    {
        if (CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_JOYSTICK) {
            CtrlMode.SetJoystickWorldPort();
        }
        else if (CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
                 CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_SCREEN_CURSOR ||
                 CtrlMode._ctrl_mode == CtrlMode.CTRL_MODE_HAND) {
            CtrlMode.SetCursorWorldPort();
        }
    }
    
    public static void SetJoystickWorldPort()
    {
        const float enlarge_factor = 1.5f;
        Vector3 world_port_lb = new Vector3();
        Vector3 world_port_rt = new Vector3();        
        CtrlMode.CalcViewportToWorldPort(ref world_port_lb, ref world_port_rt);
        CtrlMode._world_port_h = Mathf.Abs(world_port_rt.x - world_port_lb.x) * CtrlMode._joystick_enlarge_factor;
        CtrlMode._world_port_v = Mathf.Abs(world_port_rt.y - world_port_lb.y) * CtrlMode._joystick_enlarge_factor;
	
    }

    public static void SetCursorWorldPort()
    {
        Vector3 world_port_lb = new Vector3();
        Vector3 world_port_rt = new Vector3();        
        CtrlMode.CalcViewportToWorldPort(ref world_port_lb, ref world_port_rt);
        CtrlMode._world_port_h = Mathf.Abs(world_port_rt.x - world_port_lb.x);
        CtrlMode._world_port_v = Mathf.Abs(world_port_rt.y - world_port_lb.y);
    }
    
    public static void CalcViewportToWorldPort(ref Vector3 world_port_lb, ref Vector3 world_port_rt)
    {
        Camera camera = Camera.main;
        
        float view_port_r = camera.rect.xMin - CtrlMode._view_port_overflow;
        float view_port_l = camera.rect.xMax + CtrlMode._view_port_overflow;
        float view_port_t = camera.rect.yMin - CtrlMode._view_port_overflow;
        float view_port_b = camera.rect.yMax + CtrlMode._view_port_overflow;
        
        world_port_lb =
            camera.ViewportToWorldPoint(new Vector3(view_port_l,
                                                    view_port_b,
                                                    0 - camera.transform.position.z));
        world_port_rt =
            camera.ViewportToWorldPoint(new Vector3(view_port_r,
                                                    view_port_t,
                                                    0 - camera.transform.position.z));
    }

    public static void UpdateCameraViewPort(Vector3 balls_min_position,
                                            Vector3 balls_max_position)
    {
		if (Main.s_Instance.IsCameraSizeProtecting ()) {
			return;
		}

		if (AccountManager.s_bObserve) {
			return;
		}

        Camera camera = Camera.main;
        Vector3 view_port_min = balls_min_position; // camera.WorldToViewportPoint(balls_min_position);
        Vector3 view_port_max = balls_max_position; // camera.WorldToViewportPoint(balls_max_position);
        float view_port_h = Mathf.Abs(view_port_max.x - view_port_min.x);
        float view_port_v = Mathf.Abs(view_port_max.y - view_port_min.y);

        float fMaxCamSize = MapEditor.s_Instance.GetMaxCameraSize();
        if  ( view_port_h > fMaxCamSize)
        {
            view_port_h = fMaxCamSize;
        }

        if (view_port_v > fMaxCamSize)
        {
            view_port_v = fMaxCamSize;
        }

        float orthographic_size = Mathf.Max(view_port_h, view_port_v);
        orthographic_size = Mathf.Max(orthographic_size,
                                      CtrlMode.MIN_ORTHOGRAPHIC_SIZE);


		/*
		float fProtectCamSize = 0f;
		if (Main.s_Instance.IsCameraSizeProtecting (ref fProtectCamSize)) {
			orthographic_size = Mathf.Max ( orthographic_size, fProtectCamSize );
		}
		*/


        if (!Mathf.Approximately(Camera.main.orthographicSize, orthographic_size)) {
            Camera.main.orthographicSize =
                Mathf.SmoothDamp(Camera.main.orthographicSize,
                                 orthographic_size,
                                 ref CtrlMode._zoom_velocity,
                                 CtrlMode._zoom_time);
            CtrlMode.UpdateWorldPort();
        }
    }    
    
    public static int GetCtrlMode()
    {
        return CtrlMode._ctrl_mode;
    }
};
