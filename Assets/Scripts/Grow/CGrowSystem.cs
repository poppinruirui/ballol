﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CGrowSystem : MonoBehaviour {

	public static CGrowSystem s_Instance;

	public const int c_nMaxLevel = 30;

	public Dropdown _dropDownLevel;
	public InputField _inputNeedExp;
	public InputField _inputMP;
    public InputField _inputMPRecoverSpeed;
    public InputField _inputKillGainExp;
    public InputField _inputKillGainMoney;
    public InputField _inputBaseVolume;
    public InputField _inputLevelMoney;

    public Image _imgMp;
	public Text _txtMp;

	public Text _txtLevel;
	public Text _txtExp;
    public Text _txtNextLevelExp;
    public Image _imgExpPercent;

    public Text _txtMyRank;
    public Text _txtTotalPlayerNum;

    public Text _txtCurTotalVolume;
    public Text _txtEatThornNum;
    public Text _txtPlayerSpeed;
    public Text _txtJiShaInfo;
    public Text _txtAttenuate;

    public struct sLevelConfig
	{
		public int nId;
		public int fNeedExp;
		public int fMP;
        public float fMPRecoverSpeed;
        public int fKillGainExp;
        public int fKillGainMoney;// 击杀这一级玩家可以获取的金钱
        public float fBaseVolume; // 达到这一级可以加的基础体积
        public int fLevelMoney; // 达到这一级可以加的金钱
    };

	sLevelConfig m_CurLevelConfig;
	Dictionary<int, sLevelConfig> m_dicLevelConfig = new Dictionary<int, sLevelConfig>();

	int m_nCurLevel = 0;
	int m_nCurExp = 0;
	int m_nNextLevelExp = 0;

	float m_fCurMP = 0f;
	float m_fCurMaxMP = 0f;

    int m_nKillCount = 0;
    int m_nBeingKilledCount = 0;
    int m_nAssistAttackCount = 0;

    void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		InitDropDown_Level ();
		m_CurLevelConfig = GetLevelConfigById (0);
	}
	
	// Update is called once per frame
	void Update () {
        MpAutoRecover();

    }

	void InitDropDown_Level()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "没有0级哈");
		showNames.Add( "1");
		showNames.Add( "2");
		showNames.Add( "3");
		showNames.Add( "4");
		showNames.Add( "5");
		showNames.Add( "6");
		showNames.Add( "7");
		showNames.Add( "8");
		showNames.Add( "9");
		showNames.Add( "10");
		showNames.Add( "11");
		showNames.Add( "12");
		showNames.Add( "13");
		showNames.Add( "14");
		showNames.Add( "15");		
		showNames.Add( "16");
		showNames.Add( "17");
		showNames.Add( "18");
		showNames.Add( "19");
		showNames.Add( "20");
		showNames.Add( "21");
		showNames.Add( "22");		
		showNames.Add( "23");
		showNames.Add( "24");
		showNames.Add( "25");
		showNames.Add( "26");
		showNames.Add( "27");
		showNames.Add( "28");
		showNames.Add( "29");
		showNames.Add( "30");
		UIManager.UpdateDropdownView( _dropDownLevel, showNames);
	}

	sLevelConfig tempLevelConfig;
	public sLevelConfig GetLevelConfigById( int nId )
	{
		if (!m_dicLevelConfig.TryGetValue (nId, out tempLevelConfig)) {
			tempLevelConfig = new sLevelConfig ();
			tempLevelConfig.nId = nId;
			m_dicLevelConfig [nId] = tempLevelConfig;
		}
		return tempLevelConfig;
	}

	public bool CheckIsThisLevelAvailable( int nId )
	{
		return m_dicLevelConfig.TryGetValue (nId, out tempLevelConfig) && tempLevelConfig.fNeedExp > 0;
	}

	void UpdateUiContent()
	{
		_inputNeedExp.text = m_CurLevelConfig.fNeedExp.ToString();
		_inputMP.text = m_CurLevelConfig.fMP.ToString();
		_inputKillGainExp.text = m_CurLevelConfig.fKillGainExp.ToString ();
        _inputMPRecoverSpeed.text = m_CurLevelConfig.fMPRecoverSpeed.ToString();
        _inputBaseVolume.text = m_CurLevelConfig.fBaseVolume.ToString();
        _inputKillGainMoney.text = m_CurLevelConfig.fKillGainMoney.ToString();
        _inputLevelMoney.text = m_CurLevelConfig.fLevelMoney.ToString();
    }

	public void OnDropdownValueChanged_Level()
	{
		m_CurLevelConfig = GetLevelConfigById ( _dropDownLevel.value );
		UpdateUiContent ();
	}

	public void OnInputValueChanged_NeedExp()
	{
		m_CurLevelConfig.fNeedExp = int.Parse ( _inputNeedExp.text );
		m_dicLevelConfig [m_CurLevelConfig.nId] = m_CurLevelConfig;
	}

	public void OnInputValueChanged_MP()
	{
		m_CurLevelConfig.fMP = int.Parse ( _inputMP.text );
		m_dicLevelConfig [m_CurLevelConfig.nId] = m_CurLevelConfig;
	}

    public void OnInputValueChanged_MPRecoverSpeed()
    {
        m_CurLevelConfig.fMPRecoverSpeed = float.Parse(_inputMPRecoverSpeed.text);
        m_dicLevelConfig[m_CurLevelConfig.nId] = m_CurLevelConfig;
    }

    public void OnInputValueChanged_KillGainExp()
	{
		m_CurLevelConfig.fKillGainExp = int.Parse ( _inputKillGainExp.text );
		m_dicLevelConfig [m_CurLevelConfig.nId] = m_CurLevelConfig;
	}

    public void OnInputValueChanged_KillGainMoney()
    {
        m_CurLevelConfig.fKillGainMoney = int.Parse(_inputKillGainMoney.text);
        m_dicLevelConfig[m_CurLevelConfig.nId] = m_CurLevelConfig;
    }

    public void OnInputValueChanged_BaseVolume()
    {
        m_CurLevelConfig.fBaseVolume = int.Parse(_inputBaseVolume.text);
        m_dicLevelConfig[m_CurLevelConfig.nId] = m_CurLevelConfig;
    }

    public void OnInputValueChanged_LevelMoney()
    {
        m_CurLevelConfig.fLevelMoney = int.Parse(_inputLevelMoney.text);
        m_dicLevelConfig[m_CurLevelConfig.nId] = m_CurLevelConfig;
    }

    public void SaveGrow( XmlDocument xmlDoc, XmlNode node )
	{
		foreach (KeyValuePair<int, sLevelConfig> pair in m_dicLevelConfig) {
			tempLevelConfig = GetLevelConfigById (pair.Key);
			XmlNode level_node = StringManager.CreateNode (xmlDoc, node, "L" + pair.Key ); 
			StringManager.CreateNode (xmlDoc, level_node, "NeedExp", pair.Value.fNeedExp.ToString());
			StringManager.CreateNode (xmlDoc, level_node, "MP", pair.Value.fMP.ToString());
            StringManager.CreateNode(xmlDoc, level_node, "MPRecoverSpeed", pair.Value.fMPRecoverSpeed.ToString());
            StringManager.CreateNode (xmlDoc, level_node, "KillExp", pair.Value.fKillGainExp.ToString());
            StringManager.CreateNode(xmlDoc, level_node, "BaseVolume", pair.Value.fBaseVolume.ToString());
            StringManager.CreateNode(xmlDoc, level_node, "LevelMoney", pair.Value.fLevelMoney.ToString());
            StringManager.CreateNode(xmlDoc, level_node, "KillMoney", pair.Value.fKillGainMoney.ToString());
        }
	}

	public void GenerateGrow( XmlNode nodeGrow )
	{
		if (nodeGrow == null) {
			return;
		}
		for (int i = 0; i < nodeGrow.ChildNodes.Count; i++) {
			XmlNode nodeLevel = nodeGrow.ChildNodes [i];
			int nLevelId = int.Parse ( nodeLevel.Name.Substring( 1, nodeLevel.Name.Length - 1) );
			tempLevelConfig = GetLevelConfigById ( nLevelId );
			for (int j = 0; j < nodeLevel.ChildNodes.Count; j++) {
				XmlNode node_config = nodeLevel.ChildNodes [j];
				if (node_config.Name == "NeedExp") {
					tempLevelConfig.fNeedExp = int.Parse ( node_config.InnerText );
				}
				if (node_config.Name == "MP") {
					tempLevelConfig.fMP = int.Parse ( node_config.InnerText );
				}
                if (node_config.Name == "MPRecoverSpeed")
                {
                    tempLevelConfig.fMPRecoverSpeed = float.Parse(node_config.InnerText);
                }
                if (node_config.Name == "KillExp") {
					tempLevelConfig.fKillGainExp = int.Parse ( node_config.InnerText );
				}
                if (node_config.Name == "BaseVolume")
                {
                    tempLevelConfig.fBaseVolume = int.Parse(node_config.InnerText);
                }
                if (node_config.Name == "LevelMoney")
                {
                    if ( !int.TryParse(node_config.InnerText, out tempLevelConfig.fLevelMoney) )
                    {
                        tempLevelConfig.fLevelMoney = 0;
                    }
                }
                if (node_config.Name == "KillMoney")
                {
                    if ( !int.TryParse(node_config.InnerText, out tempLevelConfig.fKillGainMoney) )
                    {
                        tempLevelConfig.fKillGainMoney = 0;
                    }
                }

            } // end j
            m_dicLevelConfig [tempLevelConfig.nId] = tempLevelConfig;

		} // end i
		UpdateUiContent();
	}

	public void UpdateLevel( int nCurLevel )
	{
        int nPoints = nCurLevel - m_nCurLevel;
        m_nCurLevel = nCurLevel;
		m_CurLevelConfig = GetLevelConfigById ( m_nCurLevel );
		_txtLevel.text = m_nCurLevel.ToString ();

        // 升级了就提升蓝槽的最大值，并把蓝补满
        UpdateMaxMP();//m_fCurMaxMP = m_CurLevelConfig.fMP;

        // 升级了就加基础体积
        Main.s_Instance.m_MainPlayer.AddBaseVolumeByLevel(m_CurLevelConfig.fBaseVolume);


        // 升级了获得金钱
        Main.s_Instance.m_MainPlayer.AddMoney(m_CurLevelConfig.fLevelMoney);

        Main.s_Instance.g_SystemMsg.SetContent("达到等级" + m_nCurLevel + "，加基础体积：" + m_CurLevelConfig.fBaseVolume + ",获得金钱：" + m_CurLevelConfig.fLevelMoney);

        UpdateMP ( m_fCurMaxMP );

        CSkillSystem.s_Instance.SetTotalPoint(CSkillSystem.s_Instance.GetTotalPoint() + nPoints); // 升级就加1点“技能点数”。注意无论升到哪一级都只加1点，与等级高低无关

        int nNextLevel = m_nCurLevel + 1;
		if (nNextLevel <= c_nMaxLevel && CheckIsThisLevelAvailable (nNextLevel)) {
			m_nNextLevelExp = tempLevelConfig.fNeedExp;
			UpdateNextLevelExp (m_nNextLevelExp);
        }
        else {
			UpdateNextLevelExp ( -1 );
		}
	}

	public void DoLevelUp()
	{
		for (int i = c_nMaxLevel; i >= 2; i--) {
			if (!CheckIsThisLevelAvailable (i)) {
				continue;
			}
			if (i <= m_nCurLevel) {
				continue;
			}
			if (m_nCurExp >= tempLevelConfig.fNeedExp) {
				m_nCurExp -= tempLevelConfig.fNeedExp;
				Main.s_Instance.m_MainPlayer.SetLevel ( i );
				return;
			}
		}
	}

	public void UpdateMP( float fCurMp )
	{
        if (fCurMp < 0)
        {
            fCurMp = 0;
        }

		m_fCurMP = fCurMp;
		if (m_fCurMP > m_fCurMaxMP) {
			m_fCurMP = m_fCurMaxMP;
		}
		_txtMp.text = m_fCurMP.ToString( "f1" ) + " / " + m_fCurMaxMP;
		_imgMp.fillAmount = m_fCurMP / m_fCurMaxMP;
	}

	public void UpdateMaxMP()
	{
		m_fCurMaxMP = m_CurLevelConfig.fMP + Main.s_Instance.m_MainPlayer.GetBaseMp();
		_txtMp.text = m_fCurMP + " / " + m_fCurMaxMP;
		_imgMp.fillAmount = m_fCurMP / m_fCurMaxMP;
	}

	public float GetMP()
	{
		return m_fCurMP;
	}

	public void AddMP( float val ){
		UpdateMP ( GetMP() + val );
	}

	public void AddExp( int val )
	{
		m_nCurExp += val;
		DoLevelUp ();
		UpdateExp ( m_nCurExp );
	}

	public void UpdateExp( int val )
	{
        /*
		if (m_nNextLevelExp < 0) {
			_txtExp.text = m_nCurExp + " / -- ";
		} else {
			_txtExp.text = m_nCurExp + " / " + m_nNextLevelExp;
		}
        */
        if (m_nNextLevelExp <= 0)
        {
            _imgExpPercent.fillAmount = 0;
        }
        else
        {
            _imgExpPercent.fillAmount = (float)m_nCurExp / (float)m_nNextLevelExp;
        }
        

    }

	public void UpdateNextLevelExp( int val )
	{
        /*
		m_nNextLevelExp = val;
		if (m_nNextLevelExp < 0) {
			_txtExp.text = m_nCurExp + " / -- ";
		} else {
			_txtExp.text = m_nCurExp + " / " + m_nNextLevelExp;
		}
        */
        if (m_nNextLevelExp <= 0)
        {
            _imgExpPercent.fillAmount = 0;
        }
        else
        {
            _imgExpPercent.fillAmount = (float)m_nCurExp / (float)m_nNextLevelExp;
        }
    }

    public void SetRank( int nMainPlayerRank, int nTotalPlayerNum )
    {
        _txtMyRank.text = nMainPlayerRank.ToString();
        _txtTotalPlayerNum.text = nTotalPlayerNum.ToString();
    }

    void MpAutoRecover()
    {
        float fCurMp = GetMP();
        float fRecover = m_CurLevelConfig.fMPRecoverSpeed * Time.deltaTime;
        fCurMp += fRecover;
        if (fCurMp > m_fCurMaxMP)
        {
            fCurMp = m_fCurMaxMP;
        }
        UpdateMP(fCurMp);
    }

    public void SetCurTotalVolume( float val )
    {
        _txtCurTotalVolume.text = val.ToString(  "f2"  );
    }

    public void SetPlayerSpeed( float val )
    {
        _txtPlayerSpeed.text = val.ToString("f2");
    }

    int m_nExplodeThornNum = 0;
    public void SetEatThornNum( int val )
    {
        m_nExplodeThornNum = val;
        _txtEatThornNum.text = val.ToString();
    }

    public int GetEatThornNum()
    {
        return m_nExplodeThornNum;
    }

    public void SetKillCount( int val )
    {
        m_nKillCount = val;
        UpdateJiShaInfo();
    }

    public int GetKillCount()
    {
        return m_nKillCount;
    }

    public void SetBeingKilledCount(int val)
    {
        m_nBeingKilledCount = val;
        UpdateJiShaInfo();
    }

    public int GetBeingKilledCount()
    {
        return m_nBeingKilledCount;
    }

    public void SetAssistAttackCount( int val )
    {
        m_nAssistAttackCount = val;
        UpdateJiShaInfo();
    }

    public int GetAssistAttackCount()
    {
        return m_nAssistAttackCount;
    }

    void UpdateJiShaInfo()
    {
        _txtJiShaInfo.text = m_nKillCount + "/" + m_nBeingKilledCount + "/" + m_nAssistAttackCount;
    }
}
