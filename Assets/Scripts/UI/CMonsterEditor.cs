﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
public class CMonsterEditor : MonoBehaviour {

    public Sprite[] m_arySprites;

	public Dropdown _dropdownThorszId;
    public Dropdown _dropdownThornType;
    public Dropdown _dropdownSkill;
    public InputField _inputfieldThornDesc;
	public InputField _inputfieldThornFoodSize;
	public InputField _inputfieldSelfSize;
	public InputField _inputfieldThornColor;
	public InputField _inputfieldThornExp;
    public InputField _inputfieldThornMoney;
    public InputField _inputfieldThornBuffId;
	public InputField _inputfieldThornExplodeChildNum;
	public InputField _inputfieldThornExplodeMotherLeftPercent;
	public InputField _inputfieldThornExplodeRunDistance;
	public InputField _inputfieldThornExplodeRunTime;
	public InputField _inputfieldThornExplodeStay;
	public InputField _inputfieldThornExplodeShellTime;
	public InputField _inputfieldThornExplodeFormatioszId;
    public InputField _inputfieldThornSkillName;

    public static CMonsterEditor s_Instance;
	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {

        //m_CurThornConfig = GetThornConfigById (0);
        m_CurThornConfig = GetMonsterConfigById( 0, 0 );

        InitDropDown_ThorszId();
        InitDropDown_ThornType ();
        InitDropDown_Skill();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
		

	void InitDropDown_ThorszId()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "0号");
		showNames.Add("1号");
		showNames.Add("2号");
		showNames.Add("3号");
		showNames.Add("4号");
		showNames.Add("5号");
		showNames.Add("6号");
		showNames.Add("7号");
        showNames.Add("8号");
        showNames.Add("9号");
        showNames.Add("10号");
        showNames.Add("11号");
        showNames.Add("12号");
        showNames.Add("13号");
        showNames.Add("14号");
        showNames.Add("15号");
        showNames.Add("16号");
        showNames.Add("17号");
        showNames.Add("18号");

        UIManager.UpdateDropdownView( _dropdownThorszId, showNames);
	}

    void InitDropDown_ThornType()
    {
        List<string> showNames = new List<string>();

        showNames.Add("0 - 刺");
        showNames.Add("1 - 豆子");
        showNames.Add("2 - 场景球");
        showNames.Add("3 - 孢子");
        showNames.Add("4 - 捡拾技能");

        UIManager.UpdateDropdownView(_dropdownThornType, showNames);
    }


    void InitDropDown_Skill()
    {
        List<string> showNames = new List<string>();

        showNames.Add("潜");
        showNames.Add("刺");
        showNames.Add("湮");
        showNames.Add("免");
        showNames.Add("秒");
        showNames.Add("暴");
        showNames.Add("金");
        UIManager.UpdateDropdownView(_dropdownSkill, showNames);
    }


    public enum eMonsterBuildingType
    {
        thorn,            // 刺
        bean,             // 豆子
        scene_ball,     // 场景野球
        spore,            // 孢子
        pick_skill,            // 捡拾类技能
    };

	public struct sThornConfig
	{
		public string szId;
        public int nType;
		public float fFoodSize;
		public string szColor;
		public string szDesc;
		public float fSelfSize;
		public float fExp;
        public float fMoney;
        public float fBuffId;
		public float fExplodeChildNum;
		public float fExplodeMotherLeftPercent;
		public float fExplodeRunDistance;
		public float fExplodeRunTime;
		public float fExplodeStayTime;
		public float fExplodeShellTime;
		public float fExplodeFormationId;
        public int nSkillId;
    };

    /// <summary>
    /// / New
    /// </summary>
    Dictionary<string, sThornConfig> m_dicMonsterConfig = new Dictionary<string, sThornConfig>();
    sThornConfig tempMonsterConfig;

    public sThornConfig GetMonsterConfigById( int nType, int nId )
    {
        string szId = nType + "_" + nId;
        if (!m_dicMonsterConfig.TryGetValue(szId, out tempMonsterConfig))
        {
            tempMonsterConfig = new sThornConfig();
            tempMonsterConfig.nSkillId = CSkillSystem.SELECTABLE_SKILL_START_ID;
            tempMonsterConfig.szId = szId;
            tempMonsterConfig.nType = nType;
            m_dicMonsterConfig[szId] = tempMonsterConfig;
        }

        return tempMonsterConfig;
    }

    public sThornConfig GetMonsterConfigById( string  szId )
    {
        if ( !m_dicMonsterConfig.TryGetValue(szId, out tempMonsterConfig))
        {
            Debug.LogError( "没找到该ID号的配置： " + szId);
            return tempMonsterConfig;
        }
        string[] ary = szId.Split( '_' );
        tempMonsterConfig.nType = int.Parse(ary[0]);
        m_dicMonsterConfig[szId] = tempMonsterConfig;
        return tempMonsterConfig;
    }

    public sThornConfig NewMonsterConfig( string szId )
    {
        sThornConfig config = new sThornConfig();
        config.szId = szId;
        string[] ary = szId.Split( '_' );
        int nType = int.Parse( ary[0] );
        config.nType = nType;
        m_dicMonsterConfig[szId] = config;
        return config;
    }

    /// end New

    sThornConfig m_CurThornConfig;

    public Sprite GetSpriteByMonsterType( eMonsterBuildingType type )
    {
        return m_arySprites[(int)type];
    }
/*
	public sThornConfig GetThornConfigById( int szId )
	{
		sThornConfig config;
		if (!m_dicMonsterConfig.TryGetValue (szId, out config)) {
			config = new sThornConfig ();
			config.szId = szId;
			m_dicMonsterConfig [szId] = config;
		}
		return config;
	}
*/

    public void OnDropdownValueChanged_MonsterType()
    {
        m_CurThornConfig = GetMonsterConfigById(_dropdownThornType.value, _dropdownThorszId.value);
        UpdateContentUI();
    }

    public void OnDropdownValueChanged_ThorszId()
	{
        m_CurThornConfig = GetMonsterConfigById(_dropdownThornType.value, _dropdownThorszId.value);
        UpdateContentUI();
    }

    public void OnDropdownValueChanged_Skill()
    {
        m_CurThornConfig.nSkillId = _dropdownSkill.value + CSkillSystem.SELECTABLE_SKILL_START_ID;
        m_dicMonsterConfig[m_CurThornConfig.szId] = m_CurThornConfig;
    }


    void UpdateContentUI()
    {
        _inputfieldThornDesc.text = m_CurThornConfig.szDesc;
        _inputfieldThornFoodSize.text = m_CurThornConfig.fFoodSize.ToString();
        _inputfieldSelfSize.text = m_CurThornConfig.fSelfSize.ToString();
        _inputfieldThornColor.text = m_CurThornConfig.szColor;
        _inputfieldThornExp.text = m_CurThornConfig.fExp.ToString();
        _inputfieldThornMoney.text = m_CurThornConfig.fMoney.ToString();
        _inputfieldThornBuffId.text = m_CurThornConfig.fBuffId.ToString();
        _inputfieldThornExplodeChildNum.text = m_CurThornConfig.fExplodeChildNum.ToString();
        _inputfieldThornExplodeMotherLeftPercent.text = m_CurThornConfig.fExplodeMotherLeftPercent.ToString();
        _inputfieldThornExplodeRunDistance.text = m_CurThornConfig.fExplodeRunDistance.ToString();
        _inputfieldThornExplodeRunTime.text = m_CurThornConfig.fExplodeRunTime.ToString();
        _inputfieldThornExplodeStay.text = m_CurThornConfig.fExplodeStayTime.ToString();
        _inputfieldThornExplodeShellTime.text = m_CurThornConfig.fExplodeShellTime.ToString();
        _inputfieldThornExplodeFormatioszId.text = m_CurThornConfig.fExplodeFormationId.ToString();
        _dropdownSkill.value = m_CurThornConfig.nSkillId - CSkillSystem.SELECTABLE_SKILL_START_ID;
    }

	public void OnInputValueChanged_Thorn_SelfSize()
	{
		m_CurThornConfig.fSelfSize = float.Parse (_inputfieldSelfSize.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_FoodSize()
	{
		m_CurThornConfig.fFoodSize = float.Parse (_inputfieldThornFoodSize.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Desc()
	{
		m_CurThornConfig.szDesc = _inputfieldThornDesc.text;
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Color()
	{
		m_CurThornConfig.szColor = _inputfieldThornColor.text;
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_Exp()
	{
		m_CurThornConfig.fExp = float.Parse (_inputfieldThornExp.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

    public void OnInputValueChanged_Thorn_Money()
    {
        m_CurThornConfig.fMoney = float.Parse(_inputfieldThornMoney.text);
        m_dicMonsterConfig[m_CurThornConfig.szId] = m_CurThornConfig;
    }

    public void OnInputValueChanged_Thorn_BuffId()
	{
		m_CurThornConfig.fBuffId = float.Parse (_inputfieldThornBuffId.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeChildNum()
	{
		m_CurThornConfig.fExplodeChildNum = float.Parse (_inputfieldThornExplodeChildNum.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeMotherLeftPercent()
	{
		m_CurThornConfig.fExplodeMotherLeftPercent = float.Parse (_inputfieldThornExplodeMotherLeftPercent.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeRunDistance()
	{
		m_CurThornConfig.fExplodeRunDistance = float.Parse (_inputfieldThornExplodeRunDistance.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeRunTime()
	{
		m_CurThornConfig.fExplodeRunTime = float.Parse (_inputfieldThornExplodeRunTime.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeStayTime()
	{
		m_CurThornConfig.fExplodeStayTime = float.Parse (_inputfieldThornExplodeStay.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_ExplodeShellTime()
	{
		m_CurThornConfig.fExplodeShellTime = float.Parse (_inputfieldThornExplodeShellTime.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void OnInputValueChanged_Thorn_FormatioszId()
	{
		m_CurThornConfig.fExplodeFormationId = float.Parse (_inputfieldThornExplodeFormatioszId.text);
		m_dicMonsterConfig [m_CurThornConfig.szId] = m_CurThornConfig;
	}

	public void SaveThorn(XmlDocument xmlDoc, XmlNode node )
	{
		foreach (KeyValuePair<string, sThornConfig> pair in m_dicMonsterConfig) {
			XmlNode nodeThorn =  StringManager.CreateNode (xmlDoc, node, "T" + pair.Value.szId);
			StringManager.CreateNode (xmlDoc, nodeThorn, "ID",  pair.Value.szId );
            StringManager.CreateNode(xmlDoc, nodeThorn, "Type", pair.Value.nType.ToString());
            StringManager.CreateNode (xmlDoc, nodeThorn, "Desc",  pair.Value.szDesc );
			StringManager.CreateNode (xmlDoc, nodeThorn, "FoodSize",  pair.Value.fFoodSize.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "SelfSize",  pair.Value.fSelfSize.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "Color",  pair.Value.szColor );
			StringManager.CreateNode (xmlDoc, nodeThorn, "Exp",  pair.Value.fExp.ToString() );
            StringManager.CreateNode(xmlDoc, nodeThorn, "Money", pair.Value.fMoney.ToString());
            StringManager.CreateNode (xmlDoc, nodeThorn, "BuffId",  pair.Value.fBuffId.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeChildNum",  pair.Value.fExplodeChildNum.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeMotherLeftPercent",  pair.Value.fExplodeMotherLeftPercent.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeRunDistance",  pair.Value.fExplodeRunDistance.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeRunTime",  pair.Value.fExplodeRunTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeStayTime",  pair.Value.fExplodeStayTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeShellTime",  pair.Value.fExplodeShellTime.ToString() );
			StringManager.CreateNode (xmlDoc, nodeThorn, "ExplodeFormationId",  pair.Value.fExplodeFormationId.ToString() );
            StringManager.CreateNode(xmlDoc, nodeThorn, "SkillId", pair.Value.nSkillId.ToString());

        }
	}

	Dictionary<string, string> dicTemp = new Dictionary<string, string> ();
	public void GenerateThorn(XmlNode node)
	{
		if (node == null) {
			return;
		}

        string szVal = "";

		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode sub_node = node.ChildNodes [i];
			dicTemp.Clear ();
			for (int j = 0; j < sub_node.ChildNodes.Count; j++) {
				dicTemp[sub_node.ChildNodes[j].Name] = sub_node.ChildNodes[j].InnerText;
			} //  end j
			string szId = dicTemp["ID"];
            int nType = 0;
            string szType = "";
            if ( !dicTemp.TryGetValue("Type", out szType) )
            {
                nType = int.Parse(szType);
            }
            sThornConfig config = NewMonsterConfig(szId);
			config.szId = szId;
            config.nType = nType;
            config.szDesc = dicTemp["Desc"];
			config.fFoodSize = float.Parse (dicTemp["FoodSize"]);
			config.fSelfSize = float.Parse (dicTemp["SelfSize"]);
            if (dicTemp.TryGetValue("Money", out szVal))
            {
                config.fMoney = float.Parse(szVal   );
            }
            config.szColor = dicTemp["Color"];
			config.fExp = float.Parse (dicTemp["Exp"]);
			config.fBuffId = float.Parse (dicTemp["BuffId"]);
			config.fExplodeChildNum = float.Parse (dicTemp["ExplodeChildNum"]);
			config.fExplodeMotherLeftPercent = float.Parse (dicTemp["ExplodeMotherLeftPercent"]);
			config.fExplodeRunDistance = float.Parse (dicTemp["ExplodeRunDistance"]);
			config.fExplodeRunTime = float.Parse (dicTemp["ExplodeRunTime"]);
			config.fExplodeStayTime = float.Parse (dicTemp["ExplodeStayTime"]);
			config.fExplodeShellTime = float.Parse (dicTemp["ExplodeShellTime"]);
			config.fExplodeFormationId = float.Parse (dicTemp["ExplodeFormationId"]);

            int nSkillId = CSkillSystem.SELECTABLE_SKILL_START_ID;
            string szSkillId = "";
            if ( dicTemp.TryGetValue("SkillId", out szSkillId))
            {
                if ( int.TryParse(szSkillId, out nSkillId) )
                {

                }
            }
            config.nSkillId = nSkillId;


            m_dicMonsterConfig[szId] = config;

        } // end i
		OnDropdownValueChanged_ThorszId();
	}

    
}
