﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;



public class CAudioManager : MonoBehaviour {
    
    public static CAudioManager s_Instance;

    public AudioSource audio_main_bg;

    public AudioSource[] aryAudio;

    public enum eAudioId
    {
        e_audio_dead_shout,
        e_audio_monster_scream,
    };

    void Awake()
    {
        s_Instance = this;


    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayAudio( eAudioId id )
    {
        aryAudio[(int)id].Play();
    }
}
