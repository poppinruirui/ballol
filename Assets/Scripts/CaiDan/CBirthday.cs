﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBirthday : MonoBehaviour {

    public Button _btnGetHongBao;    
    
    public static CBirthday s_Instance;

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickButton_GetHongBao()
    {
        Main.s_Instance.m_MainPlayer.AddMoney( 1000 );
        Main.s_Instance.g_SystemMsg.SetContent( "恭喜你获得生日红包：1000点游戏币" );
        this.gameObject.SetActive( false );
    }
}
