﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_Spit : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Main m_Main;
	public static bool s_bUsingUi = false;

	bool m_bSpitting = false;
	Touch _touch;

	// Use this for initialization
	void Start ()
    {
        GameObject go;
        go = GameObject.Find("Main Camera");
        m_Main = go.GetComponent<Main>();
	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void OnPointerDown(PointerEventData evt)
	{
		if (this.gameObject.name == "btnSpit") {
			m_Main.BeginSpit ();
			s_bUsingUi = true;

			//MapEditor.s_Instance._txtDebugInfo.text = "手指按下 W";
			if (Main.s_Instance.m_nPlatform == 1) {
				Touch touch = Input.GetTouch ( Input.touchCount - 1 );
				Main.s_Instance.BeginSpitTouch (touch.fingerId, touch.position);
			}
			m_bSpitting = true;
			
		} else if (this.gameObject.name == "btnCancelSkill") {

		}else {

		}
    }

    public void OnPointerUp(PointerEventData evt)
    {
		if (this.gameObject.name == "btnSpit") {	
			m_Main.EndSpit ();
			s_bUsingUi = false;
			Main.s_Instance.EndSpitTouch ();
		} else if (this.gameObject.name == "btnCancelSkill") {
		} else {
		}



    }

	public void OnPointerEnter(PointerEventData evt)
	{
		if (this.gameObject.name == "btnSpit") {	

		} else if (this.gameObject.name == "btnCancelSkill") {

		} 
	}


	public void OnMove(PointerEventData evt)
	{
		Debug.Log ( "移动木有！！！移动木有！！！！" );


		if (this.gameObject.name == "btnSpit") {	
			
		} else if (this.gameObject.name == "btnCancelSkill") {
			
		} 
		//MapEditor.s_Instance._txtDebugInfo.text = "手指move " + _touch.position;
	}
}
